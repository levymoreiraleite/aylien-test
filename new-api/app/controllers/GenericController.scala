package controllers

import java.util.Date

import javax.inject.Inject
import models.Company
import play.api.i18n.{Lang, MessagesApi}
import play.api.mvc.{Result, _}
import repositories.{CompanyRepository, HistoryRepository}
import services.CacheService

import scala.concurrent.{ExecutionContext, Future}

/**
 * Generic wrapper controller responsible for authentication, authorization nad limits.
 */
class GenericController @Inject()(historyRepository: HistoryRepository,
                                  messagesApi: MessagesApi,
                                  cc: MessagesControllerComponents)(implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  implicit val lang: Lang = Lang(java.util.Locale.getDefault)

  def run (fn: (MessagesRequest[AnyContent], Company) => Future[Result]) = Action.async { implicit request =>
    val appId = request.headers.get("X-AYLIEN-NewsAPI-Application-ID")
    val appKey = request.headers.get("X-AYLIEN-NewsAPI-Application-Key")

    CacheService.addRequestToCacheInfo(appId, appKey) match {
      case Some(info) =>

        // Saves in the history asynchronously
        historyRepository.create(info.company.id, request.host + request.uri, new java.sql.Timestamp(new Date().getTime()))

        if (info.company.dailyLimit == 0 || info.requestsToday <= info.company.dailyLimit) fn(request, info.company)
        else Future{TooManyRequests(messagesApi("daily.limit"))}

      case None => Future{Forbidden((messagesApi("invalid.credentials")))}
    }
  }

}
