package controllers

import javax.inject._
import play.api.mvc._
import repositories.CompanyRepository
import scala.concurrent.{ExecutionContext}
import play.api.libs.ws._
import scala.concurrent.Future
import models.{OptimizerParams}
import services.PaintOptimizer

/**
 * Controller that proxy the requests to the old paint optimizer service.
 * In a real app normally we would have one package for v1 and another for v2.
 *
 */
class PaintOptimizerController @Inject()(repo: CompanyRepository,
                                         cc: MessagesControllerComponents,
                                         authService: GenericController,
                                         ws: WSClient,
                                         paintOptimizer: PaintOptimizer
                                )(implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  import authService._

  def calculateSolutionV1 = run((request, _) => {
    val input = request.getQueryString("input")

    input match {
      case Some(value) => paintOptimizer.calculateSolution(value).map {response => Ok(response.body)}
      case None => Future {BadRequest("Missing params")}
    }
  })

  /**
   * More on why I decided to move from get to post can be found here
   * https://blogs.dropbox.com/developers/2015/03/limitations-of-the-get-method-in-http
   */
  def calculateSolutionV2 = run((request, _) => {
    val optimizerParams: OptimizerParams = request.body match {
      case AnyContentAsJson(json) => json.as[OptimizerParams]
      case _ => new OptimizerParams()
    }

    if (optimizerParams.isNotEmpty)
      paintOptimizer.calculateSolutionWithObject(optimizerParams)
          .map {response => Ok(response.body)}
    else
      Future {BadRequest("Missing params")}
  })


}
