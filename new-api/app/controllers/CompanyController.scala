package controllers

import javax.inject._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.libs.json.Json
import play.api.mvc._
import repositories.{CompanyRepository, HistoryRepository}
import services.CacheService.{addCompanyToCache, getCompaniesFromCache}

import scala.concurrent.{ExecutionContext, Future}
import services.KeysGenerator._
import controllers.GenericController

class CompanyController @Inject()(repo: CompanyRepository,
                                  historyRepository: HistoryRepository,
                                  cc: MessagesControllerComponents,
                                  authService: GenericController
                                )(implicit ec: ExecutionContext)
  extends MessagesAbstractController(cc) {

  import authService._

  /**
   * The mapping for the company form.
   */
  val companyForm: Form[CreateCompanyForm] = Form {
    mapping(
      "name" -> nonEmptyText,
      "dailyLimit" -> longNumber.verifying(min(0L), max(Long.MaxValue))
    )(CreateCompanyForm.apply)(CreateCompanyForm.unapply)
  }

  /**
   * The index action.
   */
  def index = Action { implicit request =>
    Ok(views.html.company(companyForm, getCompaniesFromCache()))
  }

  /**
   * The add company action.
   *
   * This is asynchronous, since we're invoking the asynchronous methods on CompanyRepository.
   */
  def addCompany = Action.async { implicit request =>
    // Bind the form first, then fold the result, passing a function to handle errors, and a function to handle succes.
    companyForm.bindFromRequest.fold(
      // The error function. We return the index page with the error form, which will render the errors.
      // We also wrap the result in a successful future, since this action is synchronous, but we're required to return
      // a future because the company creation function returns a future.
      errorForm => {
        Future.successful(Ok(views.html.company(errorForm, null)))
      },
      // There were no errors in the from, so create the company.
      company => {
        val keys = generateKeys()
        repo.create(company.name, keys._1, keys._2, company.dailyLimit).map { newCompany =>
          addCompanyToCache(newCompany)
          Redirect(routes.CompanyController.index).flashing("success" -> "company.created")
        }
      }
    )
  }

  def getCompanies = Action { implicit request =>
    Ok(Json.toJson(getCompaniesFromCache()))
  }

  def getHistory = run( (_, company) =>
      historyRepository.list(company).map { history =>
          Ok(Json.toJson(history))
      }
  )

}

/**
 * The create company form.
 *
 * Generally for forms, you should define separate objects to your models, since forms very often need to present data
 * in a different way to your models.  In this case, it doesn't make sense to have an id parameter in the form, since
 * that is generated once it's created.
 */
case class CreateCompanyForm(name: String, dailyLimit: Long)
