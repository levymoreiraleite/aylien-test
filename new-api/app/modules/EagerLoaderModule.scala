package modules

import com.google.inject.AbstractModule
import services.StartUpService

class EagerLoaderModule extends AbstractModule {
  override def configure() = {
    bind(classOf[StartUpService]).asEagerSingleton
  }

}
