package services

import java.util.Calendar

import models.{CacheInfo, Company}

import scala.collection.mutable

/**
 * Cache that keeps the companies keys and statistics that needs fast access to validate requests.
 *
 * Ideally it should be using Redis or some sort of distributed memory cache,
 * but for simplicity we are going to use a hashmap.
 */
object CacheService {
  private val cache = new mutable.HashMap[String, CacheInfo]()

  def getCompaniesFromCache = () => cache.values.toIndexedSeq

  def addCompanyToCache = (company: Company) => {
    cache(company.appId) = CacheInfo(
      company,
      requestsToday = 0,
      day = getDayOfMonth()
    )
  }

  def addRequestToCacheInfo(appId: Option[String], appKey: Option[String]): Option[CacheInfo] = {
    cache.get(appId.getOrElse("")) match {
      case Some(info) =>
        if (info.company.appKey != appKey.getOrElse("")) None
        else {
          if (getDayOfMonth() == info.day)
            cache(appId.get) = info.copy(requestsToday = info.requestsToday + 1)
          else
            cache(appId.get) = info.copy(requestsToday = 1, day = getDayOfMonth())
          cache.get(appId.get)
        }

      case None => None
    }
  }

  def getDayOfMonth() = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
}
