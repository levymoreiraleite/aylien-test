package services

import javax.inject._
import com.typesafe.config.ConfigFactory
import models.OptimizerParams
import play.api.libs.ws._

/**
 * This service is responsible for centralize the access to the old api.
 */
class PaintOptimizer @Inject()(ws: WSClient) {

  private val apiV1Uri = ConfigFactory.load.getString("api.v1.uri")

  def calculateSolution = (input: String) => {
    ws.url(s"${apiV1Uri}?input=${input}").get()
  }

  def calculateSolutionWithObject = (input: OptimizerParams) => {
    println( s"${apiV1Uri}${input.toUri}")
    ws.url(s"${apiV1Uri}${input.toUri}").get()
  }

}
