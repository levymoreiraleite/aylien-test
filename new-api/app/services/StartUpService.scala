package services

import javax.inject.{Inject, Singleton}
import repositories.CompanyRepository
import services.CacheService.{addCompanyToCache}

import scala.concurrent.ExecutionContext

/**
 * Adds the companies to cache service.
 */
@Singleton
class StartUpService @Inject()(repo: CompanyRepository)
                              (implicit ec: ExecutionContext){

  repo.list().map(companies => {
    companies.foreach(addCompanyToCache)
  })

}
