package services

import java.util.UUID

/**
 * Extremely complex logic to generate app keys.
 */
object KeysGenerator {

  /**
   *
   * @return A tuple with the appId + appKey
   */
  def generateKeys = () => {
    val keys = UUID.randomUUID.toString.splitAt(9)
    (s"${keys._1}demo", keys._2.replaceAll("-", ""))
  }

}
