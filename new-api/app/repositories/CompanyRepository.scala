package repositories

import javax.inject.{Inject, Singleton}
import models.{Company}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
 * A repository for people.
 *
 * @param dbConfigProvider The Play db config provider.
 */
@Singleton
class CompanyRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import profile.api._

  private class CompanyTable(tag: Tag) extends Table[Company](tag, "companies") {

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def appKey = column[String]("appKey")

    def appId = column[String]("appId")

    def dailyLimit = column[Long]("dailyLimit")

    /**
     * This is the tables default "projection".
     *
     * It defines how the columns are converted to and from the object.
     */
    def * = (id, name, appKey, appId, dailyLimit) <> ((Company.apply _).tupled, Company.unapply)
  }

  /**
   * The starting point for all queries on the people table.
   */
  private val company = TableQuery[CompanyTable]

  /**
   * Create a company with the given data.
   *
   * This is an asynchronous operation, it will return a future of the created company, which can be used to obtain the
   * id for that company.
   */
  def create(name: String, appKey: String, appId: String, dailyLimit: Long): Future[Company] = db.run {
    (company.map(c => (c.name, c.appKey, c.appId, c.dailyLimit))
      returning company.map(_.id)
      into ((tuple, id) => Company(id, tuple._1, tuple._2, tuple._3, tuple._4))
    ) += (name, appId, appKey, dailyLimit)
  }

  /**
   * List all companies.
   */
  def list(): Future[Seq[Company]] = db.run {
    company.result
  }
}
