package repositories

import java.util.Date

import javax.inject.{Inject, Singleton}
import models.{Company, History}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
 * A repository for history.
 *
 * @param dbConfigProvider The Play db config provider.
 */
@Singleton
class HistoryRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {

  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private implicit val date2SqlDate = MappedColumnType.base[Date, java.sql.Timestamp]( d => new java.sql.Timestamp(d.getTime), d => new java.util.Date(d.getTime) )

  private class HistoryTable(tag: Tag) extends Table[History](tag, "history") {


    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def companyId = column[Long]("companyId")

    def uri = column[String]("uri")

    def createdAt = column[java.util.Date]("createdAt")

    /**
     * This is the tables default "projection".
     *
     * It defines how the columns are converted to and from the object.
     */
    def * = (id, companyId, uri, createdAt) <> ((History.apply _).tupled, History.unapply)
  }

  /**
   * The starting point for all queries on the people table.
   */
  private val history = TableQuery[HistoryTable]

  /**
   * Create history with the given data.
   */
  def create(companyId: Long, uri: String, createdAt: java.util.Date): Future[History] = db.run {
    (history.map(c => (c.companyId, c.uri, c.createdAt))
      returning history.map(_.id)
      into ((tuple, id) => History(id, tuple._1, tuple._2, tuple._3))
    ) += (companyId, uri, createdAt)
  }

  /**
   * List history of a company.
   */
  def list(company: Company): Future[Seq[History]] = db.run {
    history.filter(_.companyId === company.id).result
  }

}
