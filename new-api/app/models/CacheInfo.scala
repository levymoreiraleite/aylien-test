package models

import play.api.libs.json.Json

case class CacheInfo(company: Company, requestsToday: Long, day: Int)

object CacheInfo {
  implicit val cacheInfoFormat = Json.format[CacheInfo]
}
