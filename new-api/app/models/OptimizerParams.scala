package models

import play.api.libs.json.Json

case class OptimizerParams (colors: Int = 0, customers: Int = 0, demands: Array[Array[Int]] = Array()) {
  def toUri = {
    val demandsStr = this.demands.map { demand =>
      s"[${demand.mkString(",")}]"
    }.mkString(",")
    s"?input={'colors':${colors},'customers':${customers},'demands':[${demandsStr}]}"
      .replaceAll("'", "%22")
  }

  def isNotEmpty = colors != 0 && customers != 0 && demands.length >= 1
}

object OptimizerParams {
  implicit val companyFormat = Json.format[OptimizerParams]
}
