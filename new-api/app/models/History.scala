package models

import play.api.libs.json.Json

case class History(id: Long, companyId: Long, uri: String, createdAt: java.util.Date)

object History {
  implicit val companyFormat = Json.format[History]
}

