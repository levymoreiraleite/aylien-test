# Docker for new-api

Build the image (206mb)
```
docker build --no-cache -f ./docker/Dockerfile -t aylien/new-api .
```

Run detached
```
docker run -p 9000:9000 -d aylien/new-api
```

Test it 
```
wget -qO- localhost/9000
```