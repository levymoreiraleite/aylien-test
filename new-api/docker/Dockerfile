FROM openjdk:8-jdk-alpine AS build-env

ENV SCALA_VERSION="2.12.8"
ENV SBT_VERSION="0.13.12"
ENV SCALA_HOME="/usr/share/scala"
ENV SBT_HOME="/usr/share/sbt"

RUN apk add --no-cache --virtual=.build-dependencies wget ca-certificates && \
apk add --no-cache bash && \
cd "/tmp" && \
wget "https://downloads.typesafe.com/scala/${SCALA_VERSION}/scala-${SCALA_VERSION}.tgz" && \
tar xzf "scala-${SCALA_VERSION}.tgz" && \
mkdir "${SCALA_HOME}" && \
rm "/tmp/scala-${SCALA_VERSION}/bin/"*.bat && \
mv "/tmp/scala-${SCALA_VERSION}/bin" "/tmp/scala-${SCALA_VERSION}/lib" "${SCALA_HOME}" && \
ln -s "${SCALA_HOME}/bin/"* "/usr/bin/" && \
wget "http://dl.bintray.com/sbt/native-packages/sbt/${SBT_VERSION}/sbt-${SBT_VERSION}.tgz" && \
tar xzf "sbt-${SBT_VERSION}.tgz" && \
mv "/tmp/sbt" "${SBT_HOME}" && \
ln -s "${SBT_HOME}/bin/"* "/usr/bin/" && \
apk del .build-dependencies && \
rm -rf "/tmp/"*

WORKDIR /app
COPY . /app
RUN sbt dist

FROM openjdk:8-jdk-alpine
WORKDIR /app
RUN apk add --no-cache bash
COPY --from=build-env /app/target/universal/new-api-1.0.0.zip /app

RUN cd /app && unzip new-api-1.0.0.zip
CMD /app/new-api-1.0.0/bin/new-api -Dplay.http.secret.key=ad31779d4ee49d5ad5162bf1429c32e2e9933f3b
