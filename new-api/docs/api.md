# API Documentation

## Summary

* [Paint optimizer V1](#Paint-optimizer-V1)
* [Paint optimizer V2](#Paint-optimizer-V2)
* [Requests History](#Requests-History)

### Paint optimizer V1 (DEPRECATED)

Endpoint that solved the paint optimization challenge, the challenge description can be found [here](https://github.com/AYLIEN/technical_challenge).

* *GET* `/v1/?input={%22colors%22:1,%22customers%22:2,%22demands%22:[[1,1,1],[1,1,0]]}`
  * `headers`:
      * X-AYLIEN-NewsAPI-Application-ID: demo
      * X-AYLIEN-NewsAPI-Application-Key: 123
  *  `response`:
        ```javascript
        IMPOSSIBLE
        ```
   
Notice that the input data, colors, customers and demands, should be passed on the query string.

### Paint optimizer V2

On the new version of this API we are receiving a POST instead of a GET request, it will make the payload 
structure easier to understand and capable to support more data. More about this can be found [here](https://blogs.dropbox.com/developers/2015/03/limitations-of-the-get-method-in-http).

* *POST* `/v2`
    * `headers`:
        * X-AYLIEN-NewsAPI-Application-ID: demo
        * X-AYLIEN-NewsAPI-Application-Key: 123
    * `body`:
        ```
        { 
             "colors": 1,
             "customers": 2,
             "demands": [[1,1,1],[1,1,0]]}
        }
        ```
    *  `response`:
        ```javascript
        IMPOSSIBLE
        ```

### Requests History

With the introduction of the API version 2 we added a new endpoint where you can track the requests made using your API key.
Notice that the usage of this API will consume your daily limit package.  

* *GET* `/v2/history`
    * `headers`:
        * X-AYLIEN-NewsAPI-Application-ID: demo
        * X-AYLIEN-NewsAPI-Application-Key: 123
        ```
    *  `response`:
    ```javascript
    [
        {
               "id": 1,
               "companyId": 1,
               "uri": "localhost:9000/v2",
               "createdAt": 1583716944577
       }
    ]
    ```
