## Tasks

### ALY-1 
Create a new service that will communicate with the old service but Improving it
* It should use playframework and scala
* The db can be in memory for this POC
* A healthy endpoint should be created so any orchestrator knows if it's down

### ALY-2
The service should easily run on containers to facilitate Kubernetes integration (docker support)

### ALY-3
Add the possibility to create company accounts 
* Each company should have its own keys used to access the API
* Each company should have a daily limit for the number of requests that can be made
* Each request should be saved on this company history 
* Companies can access their history of requests without consuming any quota
* Admins should be able to create companies see the quotas and requests history
    
### ALY-4 
The API should be documented (ideally on with swager but let's go with markdown because of the time restriction)

### ALY-5 
Old API should be accessible behind the /v1 path 

### ALY-6
General improvements.
Just a placeholder, once we know something can go wrong and we need one or two more features not planed 

### ALY-7
NodeJS SDK should be created using the new API 

### ALY-8 
Add stress test. Use some load test tool like gartling 
*Had no time to do it with gartling so made a simple NodeJS script to test it