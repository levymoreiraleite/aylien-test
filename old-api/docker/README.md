# Docker for old-api

Build the image (206mb)
```
docker build --no-cache -f ./docker/Dockerfile -t aylien/old-api .
```

Run detached
```
docker run -p 8080:8080 -d aylien/old-api
```

Test it 
```
wget -qO- http://localhost:8080/v1/?input={%22colors%22:1,%22customers%22:2,%22demands%22:[[1,1,1],[1,1,0]]}
```