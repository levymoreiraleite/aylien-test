# Aylien Test - Levy Moreira

Sample project required by Aylien.   
The full test description can be found on [this pdf](./docs/test-description.pdf) and the list of tasks defined based on that can be found [here](./docs/tasks.md).

### IDE
I'm using IntelliJ IDEA during the development, but VSCode can also be used.
For Intellij don't import each project individually, just open the root project folder and all sub projects should 
be displayed.

### Running the project
You can run each service individually but using docker-compose we can easily start both services at the same time.  
* Docker and docker-composer are needed to run this script.
```sh
# Clone the project
git clone git@bitbucket.org:levymoreiraleite/aylien-test.git

# Start it with docker
cd aylien-test && ./scripts/run.sh
```
After that go to [localhost:9000](http://localhost:9000) and you should see the companies page like bellow.

![Companies](./docs/images/companies.png "Companies")

### Creating a company 
After start the project go to [localhost:9000](http://localhost:9000) you should see one demo company already registered.  

Let's create one new one to test the flow, first click on `Create new company` it will open a popup where you can add 
the name of the company and the requests daily limit ( start with something low so we can test it easily ;) ).

When we register a new company an unique AppKey and AppId are generated for this entity, and it will be needed to 
use the SDK or do http requests directly to the API.

#### Using the API directly
With the keys in hand we can try to do a few requests and see if the API responds as expected, and if throws an error 
when the daily limit is exceeded. 
The details about this API can be found on the [API Documentation](./new-api/docs/api.md)   
Now try to do a few requests using postman or curl to see the API behavior, remember to verify your history using
 the [requests history](./new-api/docs/api.md)

#### Using the API with the NodeJS SDK
On the folder `aylient-test/sdk/node-js` was created a small NodeJS SDK, that can be used to simplify the access to our API v2.  

To test it please have installed at least node version `8.16.1` (we are using async/await so old version will not be able to execute the code).  

Go to the node-js folder and run the following command
```sh
npm run example
```
It will execute the [example code](./sdk/node-js/examples/example.js) where we import the SDK, set the API keys and use 
it to request the optimizer calculations using callbacks and using promises.   
After that you can run 
```sh
npm run load
```   
This will run a small load test, where you can increase the number of requests and see when the old-api will break causing 
error responses coming from the new API.

   