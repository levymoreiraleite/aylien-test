#!/bin/bash

# Build the docker images
cd old-api
docker build --no-cache -f ./docker/Dockerfile -t aylien/old-api .

cd ../new-api
docker build --no-cache -f ./docker/Dockerfile -t aylien/new-api .

# Run docker-compose
cd ../docker
docker-compose up