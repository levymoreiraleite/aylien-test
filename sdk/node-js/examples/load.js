/**
 * This example runs 100 simultaneous requests, if the daily limit does not support it 
 * all HTTP 429 responses will be considered errors.
 * 
 * PS The scala server supports much much more than 100 requests at once, but the python server 
 * breaks quite easily, probably it's running on dev mode or it is just proposital.
 * If you start to get connection refused from 0.0.0.0:8080 will be necessary restart the docker
 * where the old python server is running.
 */

const aylienSdk = require('../src');

const client = aylienSdk.createClientWith({
    appId: 'demo',
    appKey: '123'
});

(async () => {
    let success = 0;
    let error = 0;

    const onSuccess = () => success++;
    const onError = () => error++;
    const promises = [];

    for (let i=0; i<100; i++) {
        const promise = client.calculatePaintOptimizer({
            customers: 2,
            colors: 1,
            demands: [[1,1,1],[1,1,0]]
        }).then(onSuccess).catch(onError);

        promises.push(promise);
    }

    await Promise.all(promises);
    console.log('success', success);
    console.log('error', error);
})();
