const aylienSdk = require('../src'); // real sdk should be published to npm of course...

const client = aylienSdk.createClientWith({
    appId: 'demo',
    appKey: '123'
});

const onSuccess = console.log;
const onError = console.error;

// Using promise 
client.calculatePaintOptimizer({
    customers: 2,
    colors: 1,
    demands: [[1,1,1],[1,1,0]]
}).then(onSuccess).catch(onError);

// Using callback
client.calculatePaintOptimizer({
    customers: 2,
    colors: 1,
    demands: [[1,1,1],[1,1,0]]
}, onSuccess, onError);
