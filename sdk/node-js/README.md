# SDK - NodeJS

NodeJS SDK to access the Aylien Paint Optimizer service.  

This SDK supports promise and callback.

## Running the example test
From the root folder of this project you can run
```
npm run example
```
It will do 2 requests using the `demo` credentials.

## Dependencies
To run this code you will need
```
Node 8.16.1
```