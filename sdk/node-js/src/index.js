/**
 * Main file where we export the SDK.
 */

const { createClientWith } = require('./factories/client');

const sdk = {
    createClientWith
};

module.exports = sdk;