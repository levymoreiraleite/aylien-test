/**
 * Factory where clients can be created defining the client keys.
 */

const { validate } = require('../services/validator');
const paintOptimizer = require('../services/paintOptimizer')

const createClientWith = (args) => {
    validate(args, ['appKey', 'appId']);
    
    return {
        calculatePaintOptimizer: paintOptimizer.createCalculate(args)
    };
};

module.exports = {
    createClientWith
};