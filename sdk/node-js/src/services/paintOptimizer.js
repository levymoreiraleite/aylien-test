/**
 * Paint optmizer service creates a function that knows how to make requests 
 * to the paint optmizer service.
 */

const { post } = require('./request');
const { validate } = require('./validator');

const createCalculate = (headers) => { // clojure to save headers
    return (payload, callbackSuccess, callbackError) => {
        validate(payload, ['colors', 'customers', 'demands']);
        return post('/v2', headers, payload, callbackSuccess, callbackError);
    }
};

module.exports = {
    createCalculate
};