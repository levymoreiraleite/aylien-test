/**
 * Service used to agregate validation functions.
 */

const validate = (args, arr) => {
    arr.forEach(element => {
        if (args[element] == undefined) 
            throw new Error(`Argument '${element}' missing.`);
    });
};

module.exports = {
    validate
};