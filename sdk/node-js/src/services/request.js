/**
 * Service that handles the requests creation.
 */

const axios = require('axios');

const domain = 'http://localhost:9000';

const post = (path, headers, payload, callbackSuccess, callbackError) => {
    const config = { 
        headers: {
        'X-AYLIEN-NewsAPI-Application-ID': headers.appId,
        'X-AYLIEN-NewsAPI-Application-Key': headers.appKey
        }   
    };
    return axios.post(`${domain}${path}`, payload, config)
      .then((result) => result.data)
      .then(callbackSuccess)
      .catch(err => {
          if (callbackError) callbackError(err.response.data)
          else throw new Error(err.response.data)
      });
}

module.exports = {
    post
}